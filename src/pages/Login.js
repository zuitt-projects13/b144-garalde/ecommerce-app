import { useState, useEffect } from 'react'
import { Form, Button, Container } from 'react-bootstrap'
import { Redirect  } from 'react-router-dom'
import Swal from 'sweetalert2' 
import {Link } from 'react-router-dom'

export default function Login() {

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(true);

	const [isLoggedIn, setIsLoggedIn] = useState(false);

	function authenticate(e){
		Swal.fire({
              title: "Login Successful",
              icon: "success",
              text: "Welcome to Zuitt!"
            })
		setIsLoggedIn(true)
	}

	useEffect(() => {
	    if(email !== '' && password !== ''){
	        setIsActive(true);
	    }
	    else{
	        setIsActive(false);
	    }
	}, [email, password])



	return (
		(isLoggedIn) ?
		    <Redirect to="/" />
		:

	    <Container id="loginForm" >
	        <h1>Login</h1>
	        
	        <Form onSubmit={(e) => authenticate(e)} className="my-3">
	            <Form.Group className="mb-3" controlId="userEmail">
	                <Form.Label>Email address</Form.Label>
	                <Form.Control 
	                    type="email" 
	                    placeholder="Enter email" 
	                    value={email}
	                    onChange={(e) => setEmail(e.target.value)}
	                    required
	                />
	            </Form.Group>

	            <Form.Group className="mb-3" controlId="password">
	                <Form.Label>Password</Form.Label>
	                <Form.Control 
	                    type="password" 
	                    placeholder="Password"
	                    value={password}
	                    onChange={(e) => setPassword(e.target.value)}
	                    required
	                />
	            </Form.Group>

	            { isActive ?
	                <Button variant="primary" type="submit" id="submitBtn" className="rounded-pill  w-75">
	                    Sign In
	                </Button>
	                :
	                <Button variant="primary" type="submit" id="submitBtn" disabled className="rounded-pill w-75">
	                    Sign In
	                </Button>
	            }
	            
	        </Form>
	        <Link to="/register">Not yet registered? Sign up now!</Link>

	    </Container>
	)
}