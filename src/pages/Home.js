import { Form, Button, Container } from 'react-bootstrap'
import { Fragment } from 'react';
import Landing from '../components/Landing'
import BestSeller from '../components/BestSeller'
import ByCategory from '../components/ByCategory'

export default function Home() {
	return (
		<Container>
		<Fragment>
			<br/>
			<Landing />
			<br/>
			<br/>
			<BestSeller />
			<br/>
			<br/>
			<ByCategory />
		</Fragment>
		</Container>
	)
}