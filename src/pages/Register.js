import { useState, useEffect } from 'react'
import { Form, Button, Container } from 'react-bootstrap'
import { Redirect  } from 'react-router-dom'
import Swal from 'sweetalert2' 
import {Link } from 'react-router-dom'




export default function Register() {


	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');	
	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);
	const [isRegistered, setIsRegistered] = useState(false);

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [age, setAge] = useState('');
	const [gender, setGender] = useState('');
	const [mobileNo, setMobileNo] = useState('');


	

	function registerUser(e){
		Swal.fire({
              title: "Register Successful",
              icon: "success",
              text: "Welcome to Zuitt!"
            })
		setIsRegistered(true)
	}

	useEffect(() => {
		if((email !== '' && password1 !== '' && password2 !== '' && firstName !== '' && lastName !== '' && age !== '' && gender !== '' && mobileNo !== '') && (password1 === password2) && (mobileNo.length === 11)){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	}, [email, password1, password2, firstName, lastName, age, gender,mobileNo])

	return (
		(isRegistered) ?
            <Redirect to="/login" />
        :


	    <Container id="registerForm" >
	        <h1>Register</h1>
	        
	       <Form className="mt-3" onSubmit={(e) => registerUser(e)}>
	       			  <Form.Group className="mb-3" controlId="userEmail">
	       			    <Form.Label>Email address</Form.Label>
	       			    <Form.Control 
	       			    	type="email" 
	       			    	placeholder="Enter email" 
	       			    	value = {email}
	       			    	onChange = { e => setEmail(e.target.value)}
	       			    	required 
	       			    />
	       			    <Form.Text className="text-muted">
	       			      We'll never share your email with anyone else.
	       			    </Form.Text>
	       			  </Form.Group>


	       			  <Form.Group className="mb-3" controlId="firstName">
	       			    <Form.Label>First Name</Form.Label>
	       			    <Form.Control 
	       			    	type="text" 
	       			    	placeholder="Enter first name" 
	       			    	value = {firstName}
	       			    	onChange = { e => setFirstName(e.target.value)}
	       			    	required 
	       			    />
	       			   </Form.Group>


	       			   <Form.Group className="mb-3" controlId="lastName">
	       			     <Form.Label>Last Name</Form.Label>
	       			     <Form.Control 
	       			     	type="text" 
	       			     	placeholder="Enter Last name" 
	       			     	value = {lastName}
	       			     	onChange = { e => setLastName(e.target.value)}
	       			     	required 
	       			     />
	       			    </Form.Group>

	       			    <Form.Group className="mb-3" controlId="age">
	       			      <Form.Label>Age</Form.Label>
	       			      <Form.Control 
	       			      	type="text" 
	       			      	placeholder="Enter age" 
	       			      	value = {age}
	       			      	onChange = { e => setAge(e.target.value)}
	       			      	required 
	       			      />
	       			     </Form.Group>

	       			     <Form.Group className="mb-3" controlId="gender">
	       			       <Form.Label>Gender</Form.Label>
	       			       <Form.Control 
	       			       	type="text" 
	       			       	placeholder="Enter gender" 
	       			       	value = {gender}
	       			       	onChange = { e => setGender(e.target.value)}
	       			       	required 
	       			       />
	       			      </Form.Group>

	       			       
	       			      <Form.Group className="mb-3" controlId="mobileNo">
	       			        <Form.Label>Mobile Number</Form.Label>
	       			        <Form.Control 
	       			        	type="text" 
	       			        	placeholder="Enter mobile number (11 digits)" 
	       			        	value = {mobileNo}
	       			        	onChange = { e => setMobileNo(e.target.value)}
	       			        	required 
	       			        />
	       			       </Form.Group>

	       			  <Form.Group className="mb-3" controlId="password1">
	       			    <Form.Label>Password</Form.Label>
	       			    <Form.Control 
	       			    	type="password" 
	       			    	placeholder="Password" 
	       			    	value={password1}
	       			    	onChange = { e => setPassword1(e.target.value)}
	       			    	required 
	       			    />
	       			  </Form.Group>
	       			  
	       			  <Form.Group className="mb-3" controlId="password2">
	       			    <Form.Label>Verify Password</Form.Label>
	       			    <Form.Control 
	       			    	type="password" 
	       			    	placeholder="Verify Password" 
	       			    	value={password2}
	       			    	onChange = { e => setPassword2(e.target.value)}
	       			    	required 
	       			    />
	       			  </Form.Group>

	            { isActive ?
	                <Button variant="primary" type="submit" id="submitBtn" className="rounded-pill  w-75">
	                    REGISTER
	                </Button>
	                :
	                <Button variant="primary" type="submit" id="submitBtn" disabled className="rounded-pill w-75">
	                    REGISTER
	                </Button>
	            }
	            
	        </Form>

	    </Container>
	)
}