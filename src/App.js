import Container from 'react-bootstrap/Container'
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch, Redirect } from 'react-router-dom';

import './App.css';
import AppNavBar from './components/AppNavBar'

import Home from './pages/Home'
import Login from './pages/Login'
import Register from './pages/Register'



function App() {
  return (
    <Router>
    <AppNavBar />
    <Container>
      <Switch>
        <Route exact path ="/" component={Home} />
        <Route exact path ="/login" component={Login}/>
        <Route exact path ="/register" component={Register} />
        <Route to="/" />
      </Switch>
    </Container>

  </Router>

  );
}

export default App;
