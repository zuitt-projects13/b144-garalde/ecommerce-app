import { Row, Card, Container, Col } from 'react-bootstrap'

export default function ByCatergory() {
	return (
		<Container>
			<h3 className="text-center">Browse by Catergory</h3>

			<Row xs={1} md={3} className="g-4">
			  {Array.from({ length: 3 }).map((_, idx) => (
			    <Col>
			      <Card>
			        <Card.Img variant="top" src="https://s3.amazonaws.com/mentoring.redesign/s3fs-public/900product.jpg" />
			        <Card.Body>
			          <Card.Title>Card title</Card.Title>
			        </Card.Body>
			      </Card>
			    </Col>
			  ))}
			</Row>
		</Container>
	)
}
