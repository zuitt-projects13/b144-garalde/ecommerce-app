import { Container, Carousel } from 'react-bootstrap'

export default function Landing() {
	return (
		<Container>
			<Carousel>
			  <Carousel.Item>
			    <img
			      className="d-block w-100"
			      src="https://www.edesk.com/wp-content/uploads/2021/03/find-trending-products-sell-ecommerce-1024x489.png"
			      alt="First slide"
			    />
			    <Carousel.Caption>
			      <h3>First slide label</h3>
			      <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
			    </Carousel.Caption>
			  </Carousel.Item>
			  <Carousel.Item>
			    <img
			      className="d-block w-100"
			      src= "https://www.edesk.com/wp-content/uploads/2021/03/find-trending-products-sell-ecommerce-1024x489.png"
			      alt="Second slide"
			    />

			    <Carousel.Caption>
			      <h3>Second slide label</h3>
			      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
			    </Carousel.Caption>
			  </Carousel.Item>
			  <Carousel.Item>
			    <img
			      className="d-block w-100"
			       src= "https://www.edesk.com/wp-content/uploads/2021/03/find-trending-products-sell-ecommerce-1024x489.png"
			      alt="Third slide"
			    />

			    <Carousel.Caption>
			      <h3>Third slide label</h3>
			      <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
			    </Carousel.Caption>
			  </Carousel.Item>
			</Carousel>
		</Container>
	)
}
