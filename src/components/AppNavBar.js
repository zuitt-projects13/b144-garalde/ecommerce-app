//search bar
import React from "react";
import { MDBCol, MDBIcon } from "mdbreact";  
import {Link, NavLink} from 'react-router-dom'

//import necessary react-bootstrap components
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import Container from 'react-bootstrap/Container'


export default function AppNavbar(){
	// State to store the user information stored in the login page
	// const [user, setUser] = useState(localStorage.getItem("email"));
	// console.log(user);

	
	return (
		<Navbar bg="light" expand="lg" >
		  <Container className="justify-content-start">
		    <Navbar.Brand as={Link} to="/" >Logo</Navbar.Brand>
		    <MDBCol md="6" className="d-flex justify-content-start">
		      <form className="form-inline mt-4 mb-4">
		        <input className="form-control form-control-md ml-3 w-85 " type="text" placeholder="Search" aria-label="Search" />
		        <MDBIcon icon="search" className="mx-2"/>
		      </form>
		    </MDBCol>
		   </Container> 
		    <Navbar.Toggle aria-controls="basic-navbar-nav" />
		    <Navbar.Collapse id="basic-navbar-nav" >
		      <Nav className="me-auto d-flex justify-content-end my-0" >
		        <Nav.Link href="#Home" className="active"><MDBIcon icon="shopping-cart" className="mx-2"/></Nav.Link>
		        <Nav.Link href="#Home" className="active"><MDBIcon icon="user" className="mx-2"/></Nav.Link>
		        <Nav.Link as={NavLink} to="/login" className="active">Login</Nav.Link>
		      </Nav>
		    </Navbar.Collapse>
		</Navbar>
	)
}
